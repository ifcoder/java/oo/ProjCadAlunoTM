
package com.oo.projcadalunotm.classes;

import java.util.Objects;

/**
 *
 * @author jose
 */
public class Aluno {
    
    private int id;
    private String nome;
    private char sexo;
    private int idade;
    private String matricula;
    

    public Aluno() {
        this.id = -1;
        this.nome = "-";
        this.sexo = '-';
        this.idade = 0;  
        this.matricula = "";
    }
    
    public Aluno(int id, String nome, char sexo, int idade, String matricula) {
        this.id = id;
        this.nome = nome;
        this.sexo = sexo;
        this.idade = idade;
        this.matricula = matricula;
    }
    
    
    public Aluno(Aluno outro){
        this.id = outro.getId();
        this.nome = outro.getNome();
        this.sexo = outro.getSexo();
        this.idade = outro.getIdade();   
        this.matricula = outro.getMatricula();
    }
    
    
    public void imprimir() {
        System.out.println(this);        
    }

    @Override
    public String toString() {
        return "Aluno{" + "id=" + id 
                + ", nome=" + nome 
                + ", sexo=" + sexo 
                + ", idade=" + idade 
                + ", matricula=" + matricula 
                + '}';
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.id;
        hash = 59 * hash + Objects.hashCode(this.matricula);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aluno other = (Aluno) obj;
        return this.id == other.id;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the sexo
     */
    public char getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    
    
}
