package com.oo.projcadalunotm.classes;

/**
 *
 * @author jose
 */
import java.util.ArrayList;
import java.util.List;

public class GerenciadorAluno {

    private List<Aluno> alunos;

    public GerenciadorAluno() {
        this.alunos = new ArrayList<>();
    }

    public void adicionarAluno(Aluno aluno) {
        alunos.add(aluno);
    }

    public boolean removerAluno(int id) {
        for (Aluno aluno : alunos) {
            if (aluno.getId() == id) {
                return alunos.remove(aluno);
            }
        }
        return false;
    }
    
    public void atualizarAluno(int idAntigo, Aluno alunoNovo) {
        Aluno produtoExistente = buscarAluno(idAntigo);
        
        if (produtoExistente != null) {
            int indice = alunos.indexOf(produtoExistente);
            alunos.set(indice, alunoNovo);
            System.out.println("Aluno atualizado com sucesso.");
        } else {
            System.out.println("Aluno cod:" + idAntigo + " não encontrado.");
        }
    }

    public Aluno buscarAluno(int id) {
        for (Aluno aluno : alunos) {
            if (aluno.getId() == id) {
                return aluno;
            }
        }
        return null;
    }
    
    public Aluno buscarPorMatricula(String matricula) {
        for (Aluno aluno : alunos) {
            if (aluno.getMatricula().equals(matricula)) {
                return aluno;
            }
        }
        return null;
    }

   
    @Override
    public String toString() {
        StringBuilder saida = new StringBuilder();
        for (Aluno aluno : alunos) {
            saida.append(aluno.toString()).append("\n");
        }
        return saida.toString();
    }
    
    
    public List<Aluno> getAlunos(){
        return alunos;
    }
}
