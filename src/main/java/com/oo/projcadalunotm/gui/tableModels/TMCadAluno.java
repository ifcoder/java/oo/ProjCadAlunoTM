
package com.oo.projcadalunotm.gui.tableModels;

import com.oo.projcadalunotm.classes.Aluno;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jose
 */
public class TMCadAluno extends AbstractTableModel {
    
     private List<Aluno> lst;
    
    private final int COL_ID = 0;
    private final int COL_NOME = 1;
    private final int COL_SEXO = 2;
    private final int COL_IDADE = 3;
    private final int COL_MATRICULA =4;
    

    public TMCadAluno(List<Aluno> lista) {
        this.lst = lista;
    }
    
    @Override
    public int getRowCount() {
        return this.lst.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }
    
    public Aluno getObjetoAluno(int row){
       return this.lst.get(row);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Aluno a = this.lst.get(rowIndex);
         if(columnIndex == COL_ID){
            return a.getId();
        }else if(columnIndex == COL_NOME){
            return a.getNome();
        }else if(columnIndex == COL_SEXO){
            return a.getSexo();
        }else if(columnIndex == COL_IDADE){
            return a.getIdade();
        }else if(columnIndex == COL_MATRICULA){
            return a.getMatricula();        
        }
        return "-";
    }
    
    
    @Override
    public String getColumnName(int columnIndex) {
        //qual o nome da coluna
         if(columnIndex == COL_ID){
            return "Id";
        }else if(columnIndex == COL_NOME){
            return "Nome";
        }else if(columnIndex == COL_SEXO){
            return "Sexo";
        }else if(columnIndex == COL_IDADE){
            return "Idade";
        }else if(columnIndex == COL_MATRICULA){
            return "Matricula";        
        }       

        return "";
    }
    
    
    
    
}
